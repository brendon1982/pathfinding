﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorithms.Domain
{
    public static class BitUtility
    {
        private static int[] _deBruinpositions = { 0, 1, 28, 2, 29, 14, 24, 3, 30, 22, 20, 15, 25, 17, 4, 8, 31, 27, 13, 23, 21, 19, 16, 7, 26, 12, 18, 6, 11, 5, 10, 9 };

        /// <summary>
        /// Returns a value that is less than or equal to the original number used to calculate bitIndex and is also
        /// a power of 2
        /// </summary>
        public static int GetPowerOfTwoFromBitIndex(int bitIndex)
        {
            return (1 << bitIndex) >> 1;
        }

        /// <summary>
        /// Returns a value that is greater than n and also a power of 2.
        /// </summary>
        public static int GetPowerOfTwoGreaterThan(int n)
        {
            if ((n & (n - 1)) == 0) return n == 0 ? 1 : (n << 1);

            n = n - 1;
            n = n | (n >> 1);
            n = n | (n >> 2);
            n = n | (n >> 4);
            n = n | (n >> 8);
            n = n | (n >> 16);
            return n + 1;
        }

        /// <summary>
        /// Returns a value that is greater than or equal to n and also a power of 2.
        /// </summary>
        public static int GetPowerOfTwoGreaterThanOrEqual(int n)
        {
            n = n - 1;
            n = n | (n >> 1);
            n = n | (n >> 2);
            n = n | (n >> 4);
            n = n | (n >> 8);
            n = n | (n >> 16);
            return n + 1;
        }

        /// <summary>
        /// Returns a value that is less than n and also a power of 2.
        /// </summary>
        /// <exception cref="ArgumentOutOfRangeException">
        /// n must be greater than 0.
        /// </exception>
        public static int GetPowerOfTwoLessThan(int n)
        {
            if ((n & (n - 1)) == 0)
            {
                if (n == 0) throw new ArgumentOutOfRangeException(nameof(n), n, "n must be greater than 0");
                return n >> 1;
            }

            n = n - 1;
            n = n | (n >> 1);
            n = n | (n >> 2);
            n = n | (n >> 4);
            n = n | (n >> 8);
            n = n | (n >> 16);

            return (n + 1) >> 1;
        }

        /// <summary>
        /// Returns a value that is less than or equal to n and also a power of 2.
        /// </summary>
        public static int GetPowerOfTwoLessThanOrEqual(int n)
        {
            if ((n & (n - 1)) == 0) return n;

            n = n - 1;
            n = n | (n >> 1);
            n = n | (n >> 2);
            n = n | (n >> 4);
            n = n | (n >> 8);
            n = n | (n >> 16);

            return (n + 1) >> 1;
        }

        /// <summary>
        /// Returns true if num has exactly one bit set.
        /// </summary>
        public static bool OneBitSet(int num)
        {
            return ((num != 0) && ((num & (num - 1)) == 0));
        }

        /// <summary>
        /// Returns true if num has exactly one bit set.
        /// </summary>
        public static bool OneBitSet(uint num)
        {
            return ((num != 0) && ((num & (num - 1)) == 0));
        }

        /// <summary>
        /// Returns true if num has exactly one bit set.
        /// </summary>
        public static bool OneBitSet(long num)
        {
            return ((num != 0) && ((num & (num - 1)) == 0));
        }

        /// <summary>
        /// Returns true if num has exactly one bit set.
        /// </summary>
        public static bool OneBitSet(ulong num)
        {
            return ((num != 0) && ((num & (num - 1)) == 0));
        }

        /// <summary>
        /// Returns the number of bits that are set according to the binary representation of n.
        /// </summary>
        /// <example>For Example:
        /// <code>
        /// Console.WriteLine(GetTotalSetBits(0));
        /// Console.WriteLine(GetTotalSetBits(2));
        /// Console.WriteLine(GetTotalSetBits(3));
        /// Console.WriteLine(GetTotalSetBits(8));
        /// Console.WriteLine(GetTotalSetBits(17));
        /// </code>
        /// results in;
        /// 0
        /// 1
        /// 2
        /// 1
        /// 2
        /// </example>
        public static int GetTotalSetBits(int n)
        {
            uint v = (uint)n;
            v = v - ((v >> 1) & 0x55555555);
            v = (v & 0x33333333) + ((v >> 2) & 0x33333333);
            return (int)(((v + (v >> 4) & 0xF0F0F0F) * 0x1010101) >> 24);
        }

        /// <summary>
        /// Returns the number of bits that are set according to the binary representation of n.
        /// </summary>
        /// <example>For Example:
        /// <code>
        /// Console.WriteLine(GetTotalSetBits(0));
        /// Console.WriteLine(GetTotalSetBits(2));
        /// Console.WriteLine(GetTotalSetBits(3));
        /// Console.WriteLine(GetTotalSetBits(8));
        /// Console.WriteLine(GetTotalSetBits(17));
        /// </code>
        /// results in;
        /// 0
        /// 1
        /// 2
        /// 1
        /// 2
        /// </example>
        public static int GetTotalSetBits(uint n)
        {
            n = n - ((n >> 1) & 0x55555555);
            n = (n & 0x33333333) + ((n >> 2) & 0x33333333);
            return (int)(((n + (n >> 4) & 0xF0F0F0F) * 0x1010101) >> 24);
        }

        /// <summary>
        /// Returns the number of bits that are set according to the binary representation of n.
        /// </summary>
        /// <example>For Example:
        /// <code>
        /// Console.WriteLine(GetTotalSetBits(0));
        /// Console.WriteLine(GetTotalSetBits(2));
        /// Console.WriteLine(GetTotalSetBits(3));
        /// Console.WriteLine(GetTotalSetBits(8));
        /// Console.WriteLine(GetTotalSetBits(17));
        /// </code>
        /// results in;
        /// 0
        /// 1
        /// 2
        /// 1
        /// 2
        /// </example>
        public static int GetTotalSetBits(long n)
        {
            ulong v = (ulong)n;
            v = v - ((v >> 1) & 0x5555555555555555);
            v = (v & 0x3333333333333333) + ((v >> 2) & 0x3333333333333333);
            return (int)(((v + (v >> 4) & 0xF0F0F0F0F0F0F0F) * 0x101010101010101) >> 56);
        }

        /// <summary>
        /// Returns the number of bits that are set according to the binary representation of n.
        /// </summary>
        /// <example>For Example:
        /// <code>
        /// Console.WriteLine(GetTotalSetBits(0));
        /// Console.WriteLine(GetTotalSetBits(2));
        /// Console.WriteLine(GetTotalSetBits(3));
        /// Console.WriteLine(GetTotalSetBits(8));
        /// Console.WriteLine(GetTotalSetBits(17));
        /// </code>
        /// results in;
        /// 0
        /// 1
        /// 2
        /// 1
        /// 2
        /// </example>
        public static int GetTotalSetBits(ulong n)
        {
            n = n - ((n >> 1) & 0x5555555555555555);
            n = (n & 0x3333333333333333) + ((n >> 2) & 0x3333333333333333);
            return (int)(((n + (n >> 4) & 0xF0F0F0F0F0F0F0F) * 0x101010101010101) >> 56);
        }

        /// <summary>
        /// Returns the first set bit (FFS), or 0 if no bits are set.
        /// </summary>
        public static int GetFirstSetBit(int number)
        {
            uint res = unchecked((uint)(number & -number) * 0x077CB531U) >> 27;
            return _deBruinpositions[res];
        }

        /// <summary>
        /// Returns the number of bits required to represent any unsigned value between zero
        /// and <paramref name="maxValue"/> - 1.
        /// </summary>
        /// <param name="maxValue">The exclusive upper bound of the range. maxValue must be greater than zero.</param>
        /// <returns>The number of bits.</returns>
        /// <exception cref="ArgumentOutOfRangeException">
        /// <paramref name="maxValue"/> must be greater than 0.
        /// </exception>
        public static int GetBitsRequired(int maxValue)
        {
            if (maxValue < 1) throw new ArgumentOutOfRangeException(nameof(maxValue), maxValue, "maxValue must be greater than 0");
            if ((maxValue & (maxValue - 1)) != 0) maxValue = GetPowerOfTwoGreaterThan(maxValue);
            uint v = (uint)(maxValue - 1);
            v = v - ((v >> 1) & 0x55555555);
            v = (v & 0x33333333) + ((v >> 2) & 0x33333333);
            return ((int)(((v + (v >> 4) & 0xF0F0F0F) * 0x1010101) >> 24));
        }

        /// <summary>
        /// Returns the number of bits you would need to shift to the left such that
        /// 1 &lt;&lt; GetBitShiftFromPowerOfTwo(<paramref name="n"/>) == <paramref name="n"/>
        /// </summary>
        /// <param name="n">A power of two that is greater than zero.</param>
        /// <returns>The number of bits.</returns>
        /// <exception cref="ArgumentOutOfRangeException">
        /// <paramref name="n"/> must be greater than 0.
        /// </exception>
        public static int GetBitShiftFromPowerOfTwo(int n)
        {
            if (n == 0) throw new ArgumentOutOfRangeException(nameof(n), n, "n must be greater than 0");
            if ((n & (n - 1)) != 0) throw new ArgumentException("n must be a power of two", nameof(n));

            uint v = (uint)(n - 1);
            v = v - ((v >> 1) & 0x55555555);
            v = (v & 0x33333333) + ((v >> 2) & 0x33333333);
            return ((int)(((v + (v >> 4) & 0xF0F0F0F) * 0x1010101) >> 24));
        }

        /// <summary>
        /// Returns the number of bits you would need to shift to the left such that
        /// 1 &lt;&lt; GetBitShiftFromPowerOfTwo(<paramref name="n"/>) == <paramref name="n"/>
        /// </summary>
        /// <param name="n">A power of two that is greater than zero.</param>
        /// <returns>The number of bits.</returns>
        /// <exception cref="ArgumentOutOfRangeException">
        /// <paramref name="n"/> must be greater than 0.
        /// </exception>
        public static int GetBitShiftFromPowerOfTwo(uint n)
        {
            if (n == 0) throw new ArgumentOutOfRangeException(nameof(n), n, "n must be greater than 0");
            if ((n & (n - 1)) != 0) throw new ArgumentException("n must be a power of two", nameof(n));

            uint v = (n - 1);
            v = v - ((v >> 1) & 0x55555555);
            v = (v & 0x33333333) + ((v >> 2) & 0x33333333);
            return ((int)(((v + (v >> 4) & 0xF0F0F0F) * 0x1010101) >> 24));
        }

        /// <summary>
        /// Returns the number of bits you would need to shift to the left such that
        /// 1 &lt;&lt; GetBitShiftFromPowerOfTwo(<paramref name="n"/>) == <paramref name="n"/>
        /// </summary>
        /// <param name="n">A power of two that is greater than zero.</param>
        /// <returns>The number of bits.</returns>
        /// <exception cref="ArgumentOutOfRangeException">
        /// <paramref name="n"/> must be greater than 0.
        /// </exception>
        public static int GetBitShiftFromPowerOfTwo(long n)
        {
            if (n == 0) throw new ArgumentOutOfRangeException(nameof(n), n, "n must be greater than 0");
            if ((n & (n - 1)) != 0) throw new ArgumentException("n must be a power of two", nameof(n));

            ulong v = (ulong)(n - 1);
            v = v - ((v >> 1) & 0x5555555555555555);
            v = (v & 0x3333333333333333) + ((v >> 2) & 0x3333333333333333);
            return ((int)(((v + (v >> 4) & 0xF0F0F0F0F0F0F0F) * 0x101010101010101) >> 56));
        }

        /// <summary>
        /// Returns the number of bits you would need to shift to the left such that
        /// 1 &lt;&lt; GetBitShiftFromPowerOfTwo(<paramref name="n"/>) == <paramref name="n"/>
        /// </summary>
        /// <param name="n">A power of two that is greater than zero.</param>
        /// <returns>The number of bits.</returns>
        /// <exception cref="ArgumentOutOfRangeException">
        /// <paramref name="n"/> must be greater than 0.
        /// </exception>
        public static int GetBitShiftFromPowerOfTwo(ulong n)
        {
            if (n == 0) throw new ArgumentOutOfRangeException(nameof(n), n, "n must be greater than 0");
            if ((n & (n - 1)) != 0) throw new ArgumentException("n must be a power of two", nameof(n));

            ulong v = (n - 1);
            v = v - ((v >> 1) & 0x5555555555555555);
            v = (v & 0x3333333333333333) + ((v >> 2) & 0x3333333333333333);
            return ((int)(((v + (v >> 4) & 0xF0F0F0F0F0F0F0F) * 0x101010101010101) >> 56));
        }

        /// <summary>
        /// Returns a value that can act as a zero based index of n, the value returned is
        /// equivalent to the number of bits you would need to shift 1 by to get the power of two that is 
        /// greater than n.
        /// </summary>
        /// <example>For Example:
        /// <code>
        /// index0 = GetBitIndex(0);
        /// index1 = GetBitIndex(1);
        /// index2 = GetBitIndex(2);
        /// index3 = GetBitIndex(3);
        /// index4 = GetBitIndex(4);
        /// </code>
        /// results in index0 set to 0, index1 set to 1, index2 set to 2, index3 set to 2 and index4 set to 3
        /// </example>
        public static int GetBitIndex(int n)
        {
            if (n == 0) return n;

            n = n | (n >> 1);
            n = n | (n >> 2);
            n = n | (n >> 4);
            n = n | (n >> 8);
            n = n | (n >> 16);

            uint v = (uint)(n - 1);
            v = v - ((v >> 1) & 0x55555555);
            v = (v & 0x33333333) + ((v >> 2) & 0x33333333);
            return ((int)(((v + (v >> 4) & 0xF0F0F0F) * 0x1010101) >> 24)) + 1;
        }

        /// <summary>
        /// Returns a value that can act as a zero based index of n, the value returned is
        /// equivalent to the number of bits you would need to shift 1 by to get the power of two that is 
        /// greater than n.
        /// </summary>
        /// <example>For Example:
        /// <code>
        /// index0 = GetBitIndex(0);
        /// index1 = GetBitIndex(1);
        /// index2 = GetBitIndex(2);
        /// index3 = GetBitIndex(3);
        /// index4 = GetBitIndex(4);
        /// </code>
        /// results in index0 set to 0, index1 set to 1, index2 set to 2, index3 set to 2 and index4 set to 3
        /// </example>
        public static int GetBitIndex(uint n)
        {
            if (n == 0) return 0;

            n = n | (n >> 1);
            n = n | (n >> 2);
            n = n | (n >> 4);
            n = n | (n >> 8);
            n = n | (n >> 16);

            uint v = (n - 1);
            v = v - ((v >> 1) & 0x55555555);
            v = (v & 0x33333333) + ((v >> 2) & 0x33333333);
            return ((int)(((v + (v >> 4) & 0xF0F0F0F) * 0x1010101) >> 24)) + 1;
        }

        /// <summary>
        /// Returns a value that can act as a zero based index of n, the value returned is
        /// equivalent to the number of bits you would need to shift 1 by to get the power of two that is 
        /// greater than n.
        /// </summary>
        /// <example>For Example:
        /// <code>
        /// index0 = GetBitIndex(0);
        /// index1 = GetBitIndex(1);
        /// index2 = GetBitIndex(2);
        /// index3 = GetBitIndex(3);
        /// index4 = GetBitIndex(4);
        /// </code>
        /// results in index0 set to 0, index1 set to 1, index2 set to 2, index3 set to 2 and index4 set to 3
        /// </example>
        public static int GetBitIndex(long n)
        {
            if (n == 0) return 0;

            n = n | (n >> 1);
            n = n | (n >> 2);
            n = n | (n >> 4);
            n = n | (n >> 8);
            n = n | (n >> 16);
            n = n | (n >> 32);

            ulong v = (ulong)(n - 1);
            v = v - ((v >> 1) & 0x5555555555555555);
            v = (v & 0x3333333333333333) + ((v >> 2) & 0x3333333333333333);
            return ((int)(((v + (v >> 4) & 0xF0F0F0F0F0F0F0F) * 0x101010101010101) >> 56)) + 1;
        }

        /// <summary>
        /// Returns a value that can act as a zero based index of n, the value returned is
        /// equivalent to the number of bits you would need to shift 1 by to get the power of two that is 
        /// greater than n.
        /// </summary>
        /// <example>For Example:
        /// <code>
        /// index0 = GetBitIndex(0);
        /// index1 = GetBitIndex(1);
        /// index2 = GetBitIndex(2);
        /// index3 = GetBitIndex(3);
        /// index4 = GetBitIndex(4);
        /// </code>
        /// results in index0 set to 0, index1 set to 1, index2 set to 2, index3 set to 2 and index4 set to 3
        /// </example>
        public static int GetBitIndex(ulong n)
        {
            if (n == 0) return 0;

            n = n | (n >> 1);
            n = n | (n >> 2);
            n = n | (n >> 4);
            n = n | (n >> 8);
            n = n | (n >> 16);
            n = n | (n >> 32);

            ulong v = (n - 1);
            v = v - ((v >> 1) & 0x5555555555555555);
            v = (v & 0x3333333333333333) + ((v >> 2) & 0x3333333333333333);
            return ((int)(((v + (v >> 4) & 0xF0F0F0F0F0F0F0F) * 0x101010101010101) >> 56)) + 1;
        }
    }
}
