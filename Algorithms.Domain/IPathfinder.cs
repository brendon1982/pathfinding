﻿using System;
using System.Text;
using System.Threading.Tasks;

namespace Algorithms.Domain
{
    public interface IPathfinder
    {
        int Steps { get; }

        void Reset(MapLocation origin, MapLocation destination);
        PathResult ComputeNextStep();
        PathResult Compute(MapLocation origin, MapLocation destination);
    }
}
