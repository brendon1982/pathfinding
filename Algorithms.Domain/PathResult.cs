﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Algorithms.Domain
{
    public class PathResult
    {
        private List<Detail> _details;

        /// <summary>
        /// Indicates whether or not the path is complete, ie: reached the destination.
        /// </summary>
        public bool IsSuccess { get; }
        /// <summary>
        /// The sequence of locations that one would navigate to follow the computed path including the original origin and destination as provided to the Compute method of the pathfinder.
        /// </summary>
        public MapLocation[] Path { get; }
        /// <summary>
        /// Locations details that should be displayed when rendering this path result.
        /// </summary>
        public IReadOnlyList<Detail> Details => _details;

        public PathResult(bool isSuccess, IEnumerable<MapLocation> path)
        {
            IsSuccess = isSuccess;
            Path = path?.ToArray() ?? new MapLocation[0];
            _details = new List<Detail>();
        }

        public void AddResultDetail(string displayName, Color4 color, IEnumerable<MapLocation> locations)
        {
            _details.Add(new Detail(displayName, color, locations.ToArray()));
        }

        public sealed class Detail
        {
            public Detail(string displayName, Color4 color, MapLocation[] locations)
            {
                DisplayName = displayName;
                Color = color;
                Locations = locations;
            }

            public string DisplayName { get; }
            public Color4 Color { get; }
            public MapLocation[] Locations { get; }
        }
    }

    public struct Color4 : IEquatable<Color4>
    {
        private static int CombineHashCodes(int h1, int h2)
        {
            return (h1 << 5) + h1 ^ h2;
        }

        public Color4(float red, float green, float blue, float alpha)
        {
            
            Red = red;
            Green = green;
            Blue = blue;
            Alpha = alpha;
        }

        public Color4(float red, float green, float blue)
        {
            Red = red;
            Green = green;
            Blue = blue;
            Alpha = 1.0f;
        }

        public readonly float Red;
        public readonly float Green;
        public readonly float Blue;
        public readonly float Alpha;

        public override int GetHashCode()
        {
            return CombineHashCodes(CombineHashCodes(CombineHashCodes(Red.GetHashCode(), Green.GetHashCode()), Blue.GetHashCode()), Alpha.GetHashCode());
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Color4))
                return false;
            return Equals((Color4)obj);
        }

        public bool Equals(Color4 other)
        {
            return Red.Equals(other.Red) && Green.Equals(other.Green) && Blue.Equals(other.Blue) && Alpha.Equals(other.Alpha);
        }

        public static bool operator ==(Color4 left, Color4 right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(Color4 left, Color4 right)
        {
            return !left.Equals(right);
        }
    }
}