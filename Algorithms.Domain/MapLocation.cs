﻿using System;
using System.Runtime.InteropServices;

namespace Algorithms.Domain
{
    [StructLayout(LayoutKind.Explicit, Size = SizeInBytes)]
    public struct MapLocation : IEquatable<MapLocation>
    {
        public const int SizeInBytes = 8;
        public static bool AllowDiagonalMovement { get; set; }

        public static MapLocation Zero => new MapLocation(0, 0);

        /// <summary>
        /// The position along the horizontal axis that this map location represents.
        /// </summary>
        [FieldOffset(0)]
        public int X;
        /// <summary>
        /// The position along the vertical axis that this map location represents.
        /// </summary>
        [FieldOffset(4)]
        public int Y;

        public MapLocation(int x, int y)
        {
            X = x;
            Y = y;
        }

        public override string ToString()
        {
            return $"[{X}, {Y}]";
        }

        public override bool Equals(object obj)
        {
            return obj is MapLocation other && Equals(other);
        }

        public bool Equals(MapLocation other)
        {
            return X == other.X &&
                   Y == other.Y;
        }

        public override int GetHashCode()
        {
            return X ^ Y;
        }

        /// <summary>
        /// Returns a new <see cref="MapLocation"/> that has been moved <paramref name="numberOfSteps"/> away relative to this <see cref="MapLocation"/> in the direction indicated by <paramref name="directionOfTravel"/>.
        /// </summary>
        /// <param name="directionOfTravel">The direction to move in, this can indicate multiple directions using flags.</param>
        /// <param name="numberOfSteps">The number of tiles to move.</param>
        /// <returns></returns>
        public MapLocation Move(MapDirection directionOfTravel, int numberOfSteps = 1)
        {
            var delta = new MapLocation();

            if ((directionOfTravel & MapDirection.North) == MapDirection.North) delta.Y++;
            if ((directionOfTravel & MapDirection.South) == MapDirection.South) delta.Y--;
            if ((directionOfTravel & MapDirection.East) == MapDirection.East) delta.X++;
            if ((directionOfTravel & MapDirection.West) == MapDirection.West) delta.X--;

            delta *= numberOfSteps;

            return this + delta;
        }

        /// <summary>
        /// Gets the direction of travel that would move towards destination.
        /// </summary>
        /// <param name="destination">The location you want to travel towards.</param>
        public MapDirection GetDirectionTo(MapLocation destination)
        {
            var delta = destination - this;
            MapDirection result = 0;

            if (delta.Y < 0) result |= MapDirection.South;
            if (delta.Y > 0) result |= MapDirection.North;
            if (delta.X < 0) result |= MapDirection.West;
            if (delta.X > 0) result |= MapDirection.East;
            
            if (!AllowDiagonalMovement && delta.X != 0 && delta.Y != 0)
            {
                if (Math.Abs(delta.X) < Math.Abs(delta.Y))
                    result &= ~(MapDirection.West | MapDirection.East);
                else
                    result &= ~(MapDirection.North | MapDirection.South);
            }

            return result;
        }


        /// <summary>
        /// Gets the direction of travel that would move towards destination.
        /// </summary>
        /// <param name="destination">The location you want to travel towards.</param>
        public MapDirection GetCardinalDirectionTo(MapLocation destination)
        {
            var delta = destination - this;
            MapDirection result = 0;

            if (delta.Y < 0) result |= MapDirection.South;
            if (delta.Y > 0) result |= MapDirection.North;
            if (delta.X < 0) result |= MapDirection.West;
            if (delta.X > 0) result |= MapDirection.East;

            if (delta.X != 0 && delta.Y != 0)
            {
                if (Math.Abs(delta.X) < Math.Abs(delta.Y))
                    result &= ~(MapDirection.West | MapDirection.East);
                else
                    result &= ~(MapDirection.North | MapDirection.South);
            }

            return result;
        }

        /// <summary>
        /// Calculates the distance between two map locations.
        /// </summary>
        /// <param name="other">The other location.</param>
        /// <returns>The distance between the two locations.</returns>
        public double DistanceBetween(MapLocation other)
        {
            var components = (this - other) * (this - other);
            return Math.Sqrt(components.X + components.Y);
        }

        public static bool operator ==(MapLocation l, MapLocation r)
        {
            return l.X == r.X && l.Y == r.Y;
        }

        public static bool operator !=(MapLocation l, MapLocation r)
        {
            return l.X != r.X || l.Y != r.Y;
        }

        public static MapLocation operator +(MapLocation l, MapLocation r)
        {
            return new MapLocation(l.X + r.X, l.Y + r.Y);
        }

        public static MapLocation operator -(MapLocation l, MapLocation r)
        {
            return new MapLocation(l.X - r.X, l.Y - r.Y);
        }

        public static MapLocation operator *(MapLocation l, int r)
        {
            return new MapLocation(l.X * r, l.Y * r);
        }

        public static MapLocation operator /(MapLocation l, int r)
        {
            return new MapLocation(l.X / r, l.Y / r);
        }
        public static MapLocation operator *(MapLocation l, MapLocation r)
        {
            return new MapLocation(l.X * r.X, l.Y * r.Y);
        }

        public static MapLocation operator /(MapLocation l, MapLocation r)
        {
            return new MapLocation(l.X / r.X, l.Y / r.Y);
        }

    }
}