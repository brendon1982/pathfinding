﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Algorithms.Domain;
using OpenGL;

namespace Algorithms.Visuals
{
    public class MapPathfindingTool : MapVisualTool
    {
        private IPathfinder _pathfinder;
        private readonly Map _map;
        private ToolStep _toolStep;
        private MapLocation _origin;
        private MapLocation _destination;
        private PathResult _computedPath;

        public bool StepMode { get; set; }
        public bool DrawDetailOverlay { get; set; }

        public event Action<string> MessageChanged;

        public MapPathfindingTool(IPathfinder pathfinder, Map map)
        {
            _pathfinder = pathfinder;
            _map = map;
        }

        protected override void OnToolActivated()
        {
            _toolStep = ToolStep.SetOrigin;
            _computedPath = null;
            MessageChanged?.Invoke("Please click anywhere in the map to select the origin of the path.");
        }


        public override bool NotifyMouseClick(MouseEventArgs args)
        {
            if (_toolStep == ToolStep.SetOrigin)
            {
                _origin = Owner.Value.ConstrainToBounds(Owner.GetMapLocationFromClient(args.Location));

                if (_map.GetTile(_origin).IsObstacle)
                {
                    MessageChanged?.Invoke("The selected location is an obstacle, Please click anywhere in the map to select the origin of the path.");
                }
                else
                {
                    _toolStep = ToolStep.SetDestination;
                    MessageChanged?.Invoke("Please click anywhere in the map to select the destination of the path.");
                }

                return true;
            }

            if (_toolStep == ToolStep.SetDestination)
            {
                var destination = Owner.Value.ConstrainToBounds(Owner.GetMapLocationFromClient(args.Location));
                if (destination == _origin)
                {
                    MessageChanged?.Invoke("Path destination cannot be the same as the origin.");
                    return false;
                }
                if (_map.GetTile(destination).IsObstacle)
                {
                    MessageChanged?.Invoke("The selected location is an obstacle, Please click anywhere in the map to select the destination of the path.");
                    return false;
                }

                _destination = destination;
                _toolStep = StepMode ? ToolStep.PathStep : ToolStep.Pathfind;
                Stopwatch timer = Stopwatch.StartNew();

                if (_toolStep == ToolStep.PathStep)
                {
                    _pathfinder.Reset(_origin, _destination);
                    _computedPath = _pathfinder.ComputeNextStep();
                }
                else
                {
                    _computedPath = _pathfinder.Compute(_origin, _destination);
                }

                timer.Stop();
                var builder = new StringBuilder();

                if (_computedPath.IsSuccess)
                {
                    _toolStep = ToolStep.Pathfind;
                    builder.AppendLine("Path computed successfully.");
                }
                else builder.AppendLine(_toolStep == ToolStep.Pathfind ? "Failed to compute path." : "Step 0");



                if (_computedPath.Path.Length > 1)
                    AppendPathStats(builder, timer.Elapsed);
                

                MessageChanged?.Invoke(builder.ToString());

                return true;
            }

            if (RunPathStep()) return true;

            return false;
        }

        public override bool NotifyKeyPress(KeyPressEventArgs args)
        {
            if (args.KeyChar == '=')
            {
                return RunPathStep();
            }

            return base.NotifyKeyPress(args);
        }

        private bool RunPathStep()
        {
            if (_toolStep == ToolStep.PathStep)
            {
                _computedPath = _pathfinder.ComputeNextStep();
                var builder = new StringBuilder();
                if (_computedPath.IsSuccess)
                {
                    _toolStep = ToolStep.Pathfind;
                    builder.AppendLine("Path computed successfully.");
                }
                else builder.AppendLine($"Step {_pathfinder.Steps}");

                if (_computedPath.Path.Length > 1)
                    AppendPathStats(builder, TimeSpan.Zero);

                MessageChanged?.Invoke(builder.ToString());

                return true;
            }

            return false;
        }

        private void AppendPathStats(StringBuilder builder, TimeSpan timeToComputePath)
        {
            var pathSpanMultipleTiles = false;
            var previous = _computedPath.Path[0];
            var totalDistanceTraveled = 0.0;
            var invalidSections = new List<MapLocation>();

            for (int i = 1; i < _computedPath.Path.Length; i++)
            {
                var current = _computedPath.Path[i];
                if (_map.GetTile(current).IsObstacle)
                {
                    invalidSections.Add(current);
                }
                
                var distanceBetween = (current - previous).DistanceBetween(MapLocation.Zero);
                totalDistanceTraveled += distanceBetween;
                if (distanceBetween > 1.9)
                {
                    pathSpanMultipleTiles = true;
                }

                previous = current;
            }


            builder.AppendLine();

            if (pathSpanMultipleTiles)
                builder.AppendLine("** The computed path has entries that are not adjacent **");

            if (invalidSections.Count > 0)
            {
                builder.AppendLine("** The computed path has entries that pass through obstacles. **");
                _computedPath.AddResultDetail("Invalid Path Sections", new Color4(1.0f, 0.6f, 0.0f, 0.6f),
                    invalidSections);
            }

            builder.AppendLine($"Total Milliseconds To Compute Path: {timeToComputePath.TotalMilliseconds}");
            builder.AppendLine($"Total Distance (Tiles): {totalDistanceTraveled}");
            builder.AppendLine($"Total Distance (Pixels): {totalDistanceTraveled * 32}");
        }

        public override void Paint(Rectangle visibleTiles, MapLocation mouseLocationOnMap)
        {
            if (_toolStep > ToolStep.SetOrigin)
            {
                if (_toolStep >= ToolStep.Pathfind && _computedPath?.Path?.Length > 0)
                {
                    Gl.BlendFunc(BlendingFactor.SrcAlpha, BlendingFactor.OneMinusSrcAlpha);
                    Gl.Enable(EnableCap.Blend);

                    if (DrawDetailOverlay)
                    {
                        Gl.Begin(PrimitiveType.Triangles);

                        foreach (var detail in _computedPath.Details)
                        {
                            foreach (var location in detail.Locations)
                            {
                                RenderQuad(Owner.TextureMap[MapTileVisual.White],
                                    Owner.GetMapLocationOnClient(location), detail.Color);
                            }
                        }

                        Gl.End();
                    }


                    Gl.Begin(PrimitiveType.Lines);
                    Gl.Color4(0.0f, 0.0f, 1.0f, 1.0f);
                    var whiteTexels = Owner.TextureMap[MapTileVisual.White];

                    var start = Owner.GetMapLocationOnClient(_computedPath.Path[0]);

                    for (int i = 1; i < _computedPath.Path.Length; i++)
                    {
                        var end = Owner.GetMapLocationOnClient(_computedPath.Path[i]);
                        Gl.TexCoord2(whiteTexels.X, whiteTexels.Y);
                        Gl.Vertex2(start.X + start.Width / 2, start.Y + start.Height / 2);

                        Gl.TexCoord2(whiteTexels.Width, whiteTexels.Height);
                        Gl.Vertex2(end.X + end.Width / 2, end.Y + end.Height / 2);

                        start = end;
                    }

                    Gl.End();
                }

                Gl.Begin(PrimitiveType.Triangles);

                RenderQuad(Owner.TextureMap[MapTileVisual.Origin], Owner.GetMapLocationOnClient(_origin), new Color4(1, 1, 1));

                if (_toolStep > ToolStep.SetDestination)
                {
                    RenderQuad(Owner.TextureMap[MapTileVisual.Destination], Owner.GetMapLocationOnClient(_destination), new Color4(1, 1, 1));
                }

                Gl.End();

                Gl.Disable(EnableCap.Blend);
            }
        }

        private void RenderQuad(RectangleF texels, Rectangle area, Color4 color)
        {
            Gl.Color4(color.Red, color.Green, color.Blue, color.Alpha);
            Gl.TexCoord2(texels.X, texels.Y);
            Gl.Vertex2(area.X, area.Y);
            Gl.TexCoord2(texels.Width, texels.Y);
            Gl.Vertex2(area.Right, area.Y);
            Gl.TexCoord2(texels.Width, texels.Height);
            Gl.Vertex2(area.Right, area.Bottom);

            Gl.TexCoord2(texels.Width, texels.Height);
            Gl.Vertex2(area.Right, area.Bottom);
            Gl.TexCoord2(texels.X, texels.Height);
            Gl.Vertex2(area.X, area.Bottom);
            Gl.TexCoord2(texels.X, texels.Y);
            Gl.Vertex2(area.X, area.Y);
        }

        private enum ToolStep
        {
            SetOrigin = 0,
            SetDestination = 1,
            Pathfind = 2,
            PathStep = 3
        }
    }
}