﻿using System.Collections.Generic;
using System.Linq;
using Algorithms.Domain;

namespace Algorithms.TestBed.Implementation
{
    public class ShadowPathfinder : IPathfinder
    {
        private int _steps;
        private readonly Map _map;
        private MapLocation _origin;
        private MapLocation _destination;
        public int Steps => _steps;

        public ShadowPathfinder(Map map)
        {
            _map = map;
        }

        public PathResult Compute(MapLocation origin, MapLocation destination)
        {
            Reset(origin, destination);
            while (true)
            {
                return new PathResult(false, new List<MapLocation> {origin, destination});
            }
        }

        // TODO what might make this even better is if the navigator.ComputeNextStep took in an alternative
        //      and point, which would be the current end point of the opposite navigator.
        public PathResult ComputeNextStep()
        {
            _steps++;



            return new PathResult(false, new List<MapLocation> {_origin, _destination});
        }

        public void Reset(MapLocation origin, MapLocation destination)
        {
            _destination = destination;
            _origin = origin;
            _steps = 0;
        }
    }
}