﻿using System.Collections.Generic;
using Algorithms.Domain;
using Priority_Queue;

namespace Algorithms.TestBed.Implementation
{
    public class PathStep : FastPriorityQueueNode
    {
        public float CostSoFar { get; }
        public float EstimatedCostToCompelete { get; }
        public float TotalCost { get; }
        public PathStep Parent { get; }
        public MapLocation Location { get; }

        public PathStep(MapLocation location, PathStep parent, float costSoFar, double estimatedCostToCompelete)
        {
            CostSoFar = costSoFar;
            EstimatedCostToCompelete = (float)estimatedCostToCompelete;
            TotalCost = CostSoFar + EstimatedCostToCompelete;
            Location = location;
            Parent = parent;
        }
    }
}