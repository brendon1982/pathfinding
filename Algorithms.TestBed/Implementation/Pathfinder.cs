﻿using Algorithms.Domain;

namespace Algorithms.TestBed.Implementation
{
    public class Pathfinder : IPathfinder
    {
        private int _steps;

        private readonly Map _map;
        private UnidirectionalPathfinder _forwardPathfinder;

        public int Steps => _steps;

        public Pathfinder(Map map)
        {
            _map = map;
        }

        public PathResult Compute(MapLocation origin, MapLocation destination)
        {
            Reset(origin, destination);
            while (true)
            {
                _steps++;

                var didForwardNavigationComplete = _forwardPathfinder.ComputeNextStep();
                if (didForwardNavigationComplete)
                {
                    return _forwardPathfinder.CreatePathResult(true);
                }
            }
        }

        public PathResult ComputeNextStep()
        {
            _steps++;
            var didForwardNavigationComplete = _forwardPathfinder.ComputeNextStep();
            return _forwardPathfinder.CreatePathResult(didForwardNavigationComplete);
        }

        public void Reset(MapLocation origin, MapLocation destination)
        {
            _steps = 0;
            _forwardPathfinder = new UnidirectionalPathfinder(_map, origin, destination);
        }
    }
}