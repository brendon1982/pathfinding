﻿using System.Collections.Generic;
using System.Linq;
using Algorithms.Domain;
using Priority_Queue;

namespace Algorithms.TestBed.Implementation
{
    public class UnidirectionalPathfinder
    {
        private readonly Map _map;
        private readonly MapLocation _destination;
        private readonly HashSet<MapLocation> _visitedLocations;

        public PathStep Path { get; }
        public FastPriorityQueue<PathStep> Paths { get; set; }

        public UnidirectionalPathfinder(Map map, MapLocation origin, MapLocation destination)
        {
            _map = map;
            _destination = destination;
            _visitedLocations = new HashSet<MapLocation>();
            Path = new PathStep(origin, null, 0, origin.DistanceBetween(destination));
            Paths = new FastPriorityQueue<PathStep>(_map.Height * _map.Width);
            Paths.Enqueue(Path, Path.TotalCost);
            _visitedLocations.Add(Path.Location);
        }

        public bool ComputeNextStep()
        {
            if (Paths.Count == 0)
            {
                return true;
            }

            var cheapestPath = Paths.Dequeue();

            var availableMovements = GetAvailableMovementsFrom(cheapestPath);

            foreach (var movement in availableMovements)
            {
                var extendedPath = AddMovementToEndOfPath(cheapestPath, movement);
                AddToPathsByCost(extendedPath);

                if (IsAtDestinatin(extendedPath))
                {
                    return true;
                }
            }

            return false;
        }

        private bool IsAtDestinatin(PathStep path)
        {
            return path.Location == _destination;
        }

        private void AddToPathsByCost(PathStep path)
        {
            Paths.Enqueue(path, path.TotalCost);
        }

        private PathStep AddMovementToEndOfPath(PathStep path, (MapLocation location, float travelCost) movement)
        {
            var costSoFar = path.CostSoFar + movement.travelCost;
            return new PathStep(movement.location, path, costSoFar, movement.location.DistanceBetween(_destination));
        }

        public PathResult CreatePathResult(bool isSuccess)
        {
            var locations = new List<MapLocation>();

            IEnumerable<PathStep> consideredPaths = Paths;
            if (isSuccess)
            {
                consideredPaths = consideredPaths.Where(step => step.Location == _destination);
            }

            var cheapestPath = consideredPaths.FirstOrDefault();
            while (cheapestPath != null)
            {
                locations.Add(cheapestPath.Location);
                cheapestPath = cheapestPath.Parent;
            }

            var result = new PathResult(isSuccess, locations.AsEnumerable().Reverse());

            //result.AddResultDetail("vistited locations", new Color4(1.0f, 1.0f, 0f, 0.5f), _visitedLocations);
            return result;
        }

        private IEnumerable<(MapLocation location, float travelCost)> GetAvailableMovementsFrom(PathStep step)
        {
            MapLocation location = step.Location;

            var north = _map.ConstrainToBounds(location.Move(MapDirection.North));
            var northEast = _map.ConstrainToBounds(location.Move(MapDirection.North | MapDirection.East));
            var east = _map.ConstrainToBounds(location.Move(MapDirection.East));
            var southEast = _map.ConstrainToBounds(location.Move(MapDirection.South | MapDirection.East));
            var south = _map.ConstrainToBounds(location.Move(MapDirection.South));
            var southWest = _map.ConstrainToBounds(location.Move(MapDirection.South | MapDirection.West));
            var west = _map.ConstrainToBounds(location.Move(MapDirection.West));
            var northWest = _map.ConstrainToBounds(location.Move(MapDirection.North | MapDirection.West));

            var northTileInfo = GetTileInfo(north);
            var northEastTileInfo = GetTileInfo(northEast);
            var eastTileInfo = GetTileInfo(east);
            var southEastTileInfo = GetTileInfo(southEast);
            var southTileInfo = GetTileInfo(south);
            var southWestTileInfo = GetTileInfo(southWest);
            var westTileInfo = GetTileInfo(west);
            var northWestTileInfo = GetTileInfo(northWest);

            if (!northTileInfo.isObsticale && _visitedLocations.Add(north)) yield return CreateStraightMovement(north, northTileInfo.travelCost);
            if ((!northTileInfo.isObsticale || !eastTileInfo.isObsticale) && !northEastTileInfo.isObsticale && _visitedLocations.Add(northEast)) yield return CreateDialognalMovement(northEast, northEastTileInfo.travelCost);
            if (!eastTileInfo.isObsticale && _visitedLocations.Add(east)) yield return CreateStraightMovement(east, eastTileInfo.travelCost);
            if ((!southTileInfo.isObsticale || !eastTileInfo.isObsticale) && !southEastTileInfo.isObsticale && _visitedLocations.Add(southEast)) yield return CreateDialognalMovement(southEast, southEastTileInfo.travelCost);
            if (!southTileInfo.isObsticale && _visitedLocations.Add(south)) yield return CreateStraightMovement(south, southTileInfo.travelCost);
            if ((!southTileInfo.isObsticale || !westTileInfo.isObsticale) && !southWestTileInfo.isObsticale && _visitedLocations.Add(southWest)) yield return CreateDialognalMovement(southWest, southWestTileInfo.travelCost);
            if (!westTileInfo.isObsticale && _visitedLocations.Add(west)) yield return CreateStraightMovement(west, westTileInfo.travelCost);
            if ((!northTileInfo.isObsticale || !westTileInfo.isObsticale) && !northWestTileInfo.isObsticale && _visitedLocations.Add(northWest)) yield return CreateDialognalMovement(northWest, northWestTileInfo.travelCost);
        }

        private static (MapLocation location, float directionFactor) CreateStraightMovement(MapLocation north, int travelCost)
        {
            return (north, travelCost);
        }

        private static (MapLocation location, float directionFactor) CreateDialognalMovement(MapLocation north, int travelCost)
        {
            return (north, 1.414f * travelCost);
        }

        private (bool isObsticale, int travelCost) GetTileInfo(MapLocation location)
        {
            var constrainToBounds = _map.ConstrainToBounds(location);
            if (location != constrainToBounds)
            {
                return (true, int.MaxValue);
            }

            var mapTile = _map.GetTile(location);
            return (mapTile.IsObstacle, mapTile.TravelCost);
        }
    }
}